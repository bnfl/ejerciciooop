import unittest
from Coche import Coche

class TestCoche (unittest.TestCase):
	def test_acelera_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.acelera_coche(30), 100)

	def test_acelera_coche_con_valores_incorrectos(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertNotEqual(coche1.acelera_coche(25), 60)
	
	def test_frena_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.frena_coche(40), 30)

	def test_frena_coche_con_valores_incorrectos(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertNotEqual(coche1.frena_coche(55), 40)
	
	def test_color_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.color_coche(), "El color del coche es azul")
	
	def test_marca_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.marca_coche(), "La marca del coche es Toyota")
			
	def test_modelo_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.modelo_coche(), "El modelo del coche es Auris")
	
	def test_matricula_coche(self):
		coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 70)
		self.assertEqual(coche1.matricula_coche(), "La matricula del coche es 1942-LRM")

if __name__ == '__main__':
	unittest.main()