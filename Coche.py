class Coche:
	def __init__ (self, col, marc, mod, matr, vel):
		self.color = col
		self.marca = marc
		self.modelo = mod
		self.matricula = matr
		self.velocidad = vel
	
	def acelera_coche (self, acl):
		self.velocidad += acl
		return self.velocidad
	
	def frena_coche (self, frn):
		self.velocidad -= frn
		return self.velocidad
	
	def color_coche (self):
		return "El color del coche es {color}".format(color = self.color)
		
	def marca_coche (self):
		return "La marca del coche es {marca}".format(marca = self.marca)
		
	def modelo_coche (self):
		return "El modelo del coche es {modelo}".format(modelo = self.modelo)	
		
	def matricula_coche (self):
		return "La matricula del coche es {matricula}".format(matricula = self.matricula)
		
"""coche1 = Coche("azul", "Toyota", "Auris", "1942-LRM", 10)

print(coche1.acelera_coche(20))"""
